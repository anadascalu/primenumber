
package prim;
import java.util.Scanner;


public class Prim {

    boolean isPrime(int n) {
    
    if (n%2==0) return false;
    
    for(int i=3;i*i<=n;i+=2) {
        if(n%i==0)
            return false;
    }
    return true;
    }
    
    public static void main(String[] args)  throws java.lang.Exception{
        boolean isPrime = false;
        
        Scanner in = new Scanner (System.in); 
        int number; 
        System.out.println ("enter a number"); 
        number=in.nextInt(); 
        
        int nrPrim = number + 1;
        while (isPrime != true)
        {   
            isPrime = true;
            if (nrPrim % 2 == 0) 
            {
                isPrime = false;
                nrPrim ++;
            }
            if (isPrime == true)
                for(int i=3; i*i <= nrPrim; i+=2) {
                    if(nrPrim % i == 0)
                    {
                        isPrime = false;
                        nrPrim ++;
                        break;
                    }
                }
            if (isPrime == true)
                System.out.println(nrPrim + " is your prime number");
        }
        
        

    }
    
}
